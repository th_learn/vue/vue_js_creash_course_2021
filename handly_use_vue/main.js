console.log('main.js imported')
const app = Vue.createApp({
    data() {
        return {
            firstName: '',
            lastName: '',
            email: '',
            gender: '',
            picture: '',
        }
    },
    methods: {
        async getUser() {
            console.log('getUser called')
            const res = await fetch('https://randomuser.me/api')

            const { results } = await res.json()

            const user = results[0]
            this.firstName = user.name.first
            this.lastName = user.name.last
            this.email = user.email
            this.gender = user.gender
            this.picture = user.picture.large
        }
    }
})


console.log(app)

app.mount("#app")
